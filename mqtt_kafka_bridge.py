import paho.mqtt.client as mqtt
from pykafka import KafkaClient
import time

# Configuracao do mosquitto
mqtt_broker = "mqtt.eclipseprojects.io"
mqtt_client = mqtt.Client("BridgeMQTT2Kafka")
mqtt_client.connect(mqtt_broker)

# Configuraçcao do Kafka
# Substituir "localhost:9092" caso utilizar outra maquina para o Kafka
kafka_client = KafkaClient(hosts="localhost:9092")
kafka_topic = kafka_client.topics['temperature']
kafka_producer = kafka_topic.get_sync_producer()

def on_message(client, userdata, message):
    msg_payload = str(message.payload)
    print("Mensagem MQTT Recebida: ", msg_payload)
    kafka_producer.produce(msg_payload.encode('ascii'))
    print("KAFKA: Publicado " + msg_payload + " para o topico temperature")

mqtt_client.loop_start()
mqtt_client.subscribe("temperature")
mqtt_client.on_message = on_message
time.sleep(300)
mqtt_client.loop_end()
