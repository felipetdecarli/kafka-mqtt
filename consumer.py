from kafka import KafkaConsumer
import boto3
import uuid
from datetime import datetime

dynamodb = boto3.resource('dynamodb')

consumer = KafkaConsumer(
    'temperature',
    bootstrap_servers=['localhost:9092'],
     auto_offset_reset='earliest',     enable_auto_commit=True,
     group_id='my-group')


for message in consumer:
    print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                          message.offset, message.key,
                                          message.value))

KafkaConsumer(auto_offset_reset='earliest', enable_auto_commit=False)

KafkaConsumer(value_deserializer=lambda m: putObject(m.decode('ascii')))

# consume msgpack
KafkaConsumer(value_deserializer=msgpack.unpackb)

# StopIteration if no message after 1sec
KafkaConsumer(consumer_timeout_ms=1000)

def putObject (temperature):
    table = dynamodb.Table('temperature')
    table.put_item(
    Item={
            'uuid': str(uuid.uuid4()),
            'temperature': temperature,
            'timestamp': datetime.timestamp(now),
        }
    )
