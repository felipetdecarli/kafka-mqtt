# Kafka-Mqtt 

 

Kafka-Mqtt é um projeto em desenvolvimento para a disciplina SSC0158-Computação em Nuvem, da Universidade de São Paulo.

  

## Introdução

O projeto utiliza as tecnologias Kafka e MQTT Mosquitto, para realizar uma simulação de um stream de dados em tempo real.

O Mosquitto é responsável por realizar a transferência de dados do dispositivo IOT simulado, para o Kafka.

Inicialmente, foi utilizado duas máquinas (2vCPU, 1GiB RAM), uma com o Kafka instalado, e a outra com o Mosquitto Broker. As máquinas possuíam o OS Amazon Linux 2.

Os arquivos `iot_data_gen.py` e `mqtt_kafka_bridge.py` são responsáveis por gerar os dados do IOT simulado, e enviar esses dados para o Kafka, respectivamente.

Os dados simulados são de um IOT medidor de temperatura, onde há uma variação aleatória entre 20 á 21ºC.
  

## Instalação

 Na primeira máquina, é necessário instalar o Java 8 através do seguinte comando:

```bash
sudo yum install java-1.8.0-openjdk
```
Para instalar o Kafka, deve ser utilizado os seguintes comandos:
```bash
wget https://ftp.unicamp.br/pub/apache/kafka/2.8.0/kafka_2.13-2.8.0.tgz
tar -xzf kafka_2.13-2.8.0.tgz
cd kafka_2.13-2.8.0/
```
Para iniciar o Zookeper, inserir o comando:
```bash
bin/zookeeper-server-start.sh config/zookeeper.properties
```
Em um outro terminal, insira o comando a seguir para inicializar o Kafka:
```bash
bin/kafka-server-start.sh config/server.properties
```
Novamente em outro terminal, crie um tópico denominado *temperature*:
```bash
bin/kafka-topics.sh --create --topic temperature --bootstrap-server localhost:9092
```
Na outra máquina, inserir os seguintes comandos para obter os arquivos de geração de dados e conexão do Mosquitto com o Kafka:
```bash
sudo yum update
sudo yum install git
pip3 install pykafka
pip3 install paho-mqtt
git clone https://gitlab.com/felipetdecarli/kafka-mqtt
cd kafka-mqtt
```
É necessário editar o arquivo `mqtt_kafka_bridge.py` e substituir `localhost:9092` pelo IP privado da primeira máquina.

Insira o seguinte comando:
```bash
python3 iot_data_gen.py
```
Em outro terminal, insira o comando:
 ```bash
python3 mqtt_kafka_bridge.py
```



## Autores

Felipe Tiago De Carli - felipedecarli@usp.br - NUSP: 10525686  
Rafael Gongora Bariccatti - bariccatti@usp.br - NUSP: 10892273
