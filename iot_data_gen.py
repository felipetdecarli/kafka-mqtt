import paho.mqtt.client as mqtt
from random import uniform
import time

# Configuracao do mosquitto broker
mqtt_broker = "mqtt.eclipseprojects.io"
mqtt_client = mqtt.Client("Temperature_Inside")
mqtt_client.connect(mqtt_broker)

while True:
    # Obtem uma temperatura aleatoria, entre 20 a 21 graus
    randNumber = uniform(20.0, 21.0)
    # Publica para o topico temperature
    mqtt_client.publish("temperature", randNumber)
    print("MQTT Publicado " + str(randNumber) + " para o topico temperatura")
    # Aguarda para a proxima publicacao
    time.sleep(3)
